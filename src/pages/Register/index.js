import React, { useState } from 'react';
import { Button, TextField, Box, Container, Snackbar } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { registerUser } from '../../providers/user';

import TextCPF from '../../components/TextCPF';

export default function Register({ history }) {
	const classes = useStyles();

	const [data, setData] = useState(new Date());
	const [nome, setNome] = useState(null);
	const [email, setEmail] = useState(null);
	const [cpf, setCPF] = useState(null);
	const [senha, setSenha] = useState(null);
	const [confirmacao, setConfirmacao] = useState(null);

	const [message, setMessage] = React.useState('');
	const [open, setOpen] = React.useState(false);
	const [okay, setOkay] = React.useState(false);

	function handleSubmit(event) {
		event.preventDefault();

		if (senha !== confirmacao) {
			setMessage('As senhas não combinam, tente digitar novamente');
			setOpen(true);
			return;
		}

		const user = { nome, email, cpf, data, senha };
		registerUser(user).then(response => {
			if (response.status === 201) {
				setMessage('Usuário criado!');
				setOkay(true);
				setOpen(true);
			}
		});
	}

	return (
		<MuiPickersUtilsProvider utils={DateFnsUtils}>
			<Box height='100vh' display='flex' flexDirection='column' alignItems='center' justifyContent='center'>
				<img src='logo128.png' alt='Logo' width='128' />
				<Container>
					<form onSubmit={handleSubmit}>
						<TextField margin='normal' required fullWidth label='Nome' onChange={event => setNome(event.target.value)} />
						<TextCPF fullWidth onChange={event => setCPF(event.target.value)} />
						<KeyboardDatePicker margin='normal' fullWidth label='Data' format='dd/MM/yyyy' onChange={value => setData(value)} KeyboardButtonProps={{ 'aria-label': 'mudar data', }} value={data} />
						<TextField margin='normal' required fullWidth type='email' label='E-mail' onChange={event => setEmail(event.target.value)} />
						<TextField margin='normal' required fullWidth label='Senha' type='password' onChange={event => setSenha(event.target.value)} />
						<TextField margin='normal' required fullWidth label='Confirmar senha' type='password' onChange={event => setConfirmacao(event.target.value)} />
						<Button type='submit' fullWidth variant='contained' className={classes.submit} color='primary'>Cadastrar</Button>
					</form>
				</Container>
			</Box>
			<Snackbar
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'center',
				}}
				className={classes.snackbar}
				open={open}
				autoHideDuration={2000}
				onClose={() => { setOpen(false); if (okay) { history.push('/'); } }}
				ContentProps={{
					'aria-describedby': { message },
				}}
				message={message}
			/>
		</MuiPickersUtilsProvider>
	);
}

const useStyles = makeStyles(theme => ({
	submit: {
		margin: theme.spacing(4, 0, 2),
	},
	snackbar: {
		bottom: theme.spacing(9),
	},
}));