import React, { useState, useEffect } from 'react';

import { Divider, ListItemSecondaryAction, Container, IconButton, ListItemText, Typography, Checkbox, List, Fab, ListItemIcon, ListItem, Box } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import { Edit, Add } from '@material-ui/icons';

import { listNotes, checkNote } from '../../providers/notes';



export default function ListNotes({ history, filtrar }) {
	const classes = useStyles();
	const [notes, setNotes] = useState(null);


	async function handleToggle(id) {
		checkNote(id).then((response) => {

			if (response.status === 200) {
				response.notes.map(note => JSON.parse(note));
			}

		});
	}

	function newNote() {
		history.push('/new-note');
	}


	useEffect(() => {
		async function loadNotes() {
			listNotes().then(response => {
				if (response.notes.length > 0) {
					let result = response.notes.map((note) => {
						note = JSON.parse(note);
						note.data = new Date(note.data);
						return note;
					});

					result = result.filter(note => {
						return note.concluido === filtrar;
					});
					result.sort((a, b) => {
						return a.data - b.data;
					});

					setNotes(result);

				}
			});
		}


		loadNotes();

	});


	return notes ? (
		<Container>
			<List>
				{notes.map((note) => {
					return (
						<div>
							<ListItem key={note.id} dense button onClick={() => handleToggle(note.id)}>
								<ListItemIcon>

									<Checkbox
										edge='start'
										checked={note.concluido === true}
										tabIndex={-1}
										disableRipple
										inputProps={{ 'aria-labelledby': note.id }}
									/>
								</ListItemIcon>
								<ListItemText primary={note.titulo + ' | ' + note.data.toLocaleDateString()} className={classes.itemText}
									secondary={
										<React.Fragment>

											<Typography
												component='span'
												variant='body2'
												color='textPrimary'
											>
												{note.descricao}
											</Typography>
										</React.Fragment>
									}
								/>
								<ListItemSecondaryAction onClick={() => history.push(`/edit-note/${note.id}`)}>
									<IconButton edge='end' aria-label='Editar'>
										<Edit />
									</IconButton>
								</ListItemSecondaryAction>
							</ListItem>
							<Divider variant='inset' component='li' />
						</div>
					)
				}
				)}

			</List>
			<Fab color='primary' aria-label='novo' className={classes.add} onClick={() => newNote()}>
				<Add />
			</Fab>
		</Container >
	) : (
			< Box height='100%' display='flex' flexDirection='column' alignItems='center' justifyContent='center'>
				<Typography variant='h4'>
					Não há anotações
				</Typography>
				<Fab color='primary' aria-label='novo' className={classes.add} onClick={() => newNote()}>
					<Add />
				</Fab>
			</Box >
		);
}

const useStyles = makeStyles(theme => ({
	add: {
		position: 'fixed',
		bottom: theme.spacing(8),
		right: theme.spacing(2),
	},

	itemText: {
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap',
		overflow: 'hidden',

	},
}));
