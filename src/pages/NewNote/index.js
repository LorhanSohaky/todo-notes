import React from 'react';

import { Container, AppBar, Toolbar, IconButton, Typography, Grid, TextField, Fab, Snackbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { ArrowBack, Done } from '@material-ui/icons';

import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { newNote, getNote, updateNote } from '../../providers/notes';


export default function NewNote({ history, match }) {
	const classes = useStyles();
	const id = match.params.id;

	const [data, setData] = React.useState(new Date());
	const [titulo, setTitulo] = React.useState('');
	const [descricao, setDescricao] = React.useState('');
	const [open, setOpen] = React.useState(false);
	const [message, setMessage] = React.useState('');
	const [valid, setValid] = React.useState(true);
	const [load, setLoad] = React.useState(false);

	const title = id ? 'Editar anotação' : 'Cadastrar anotação';

	function prepareToSend() {
		setTitulo(titulo.trim());
		setDescricao(descricao.trim());
	}

	function updateClick() {
		prepareToSend();
		let note = { data, titulo, descricao };
		setValid(isValid());
		if (isValid()) {
			updateNote(id, note).then((response) => {
				if (response.status === 200) {
					showSnackbar('Atualizado com sucesso!');
				}
			});
		} else {
			showSnackbar('Preencha o campo de título');
		}
	};

	function addClick() {
		let note = { data, titulo, descricao };
		setValid(isValid());
		if (isValid()) {
			newNote(note).then((response) => {
				if (response.status === 201) {
					showSnackbar('Criado com sucesso!');
				}
			});
		} else {
			showSnackbar('Preencha o campo de título');
		}
	};

	if (id != null) {
		if (!load) {
			getNote(id).then(response => {
				let note = JSON.parse(response.note);
				setData(new Date(note.data));
				setTitulo(note.titulo);
				setDescricao(note.descricao);
				setLoad(true);
			});
		}

	}

	function handleDataChange(value) {
		setData(value);
	}

	function handleTituloChange(novoTitulo) {
		setTitulo(novoTitulo.target.value);
	}

	function handleDescricaoChange(novaDescricao) {
		setDescricao(novaDescricao.target.value);
	}

	function showSnackbar(message) {
		setMessage(<span>{message}</span>);
		setOpen(true);
	}

	function isValid() {
		return titulo.length > 0;
	}

	let tituloField;

	if (valid) {
		tituloField = <TextField label='Título' required placeholder='ex. Fazer compras de mercado' margin='normal' onChange={handleTituloChange} value={titulo} />;
	} else {
		tituloField = <TextField label='Título' required placeholder='ex. Fazer compras de mercado' margin='normal' onChange={handleTituloChange} error value={titulo} helperText='Esse campo é obrigatório' />;
	}

	return (
		<MuiPickersUtilsProvider utils={DateFnsUtils}>
			<AppBar position='static'>
				<Toolbar>
					<IconButton edge='start' arial-label='Voltar para a tela anterior' onClick={() => { history.push('/') }}>
						<ArrowBack />
					</IconButton>

					<Typography variant='h6' className={classes.title}>
						{title}
					</Typography>
				</Toolbar>
			</AppBar>
			<Container className={classes.container}>
				<Grid container direction='column' justify='flex-start' alignItems='stretch'>
					{tituloField}
					<TextField label='Descrição' placeholder='ex. Comprar morango, tomate e maracujá.' multiline margin='normal' onChange={handleDescricaoChange} value={descricao} />
					<KeyboardDatePicker margin='normal' label='Data' format='dd/MM/yyyy' onChange={handleDataChange} KeyboardButtonProps={{ 'aria-label': 'mudar data', }} value={data} />
				</Grid>
				<Snackbar
					anchorOrigin={{
						vertical: 'bottom',
						horizontal: 'center',
					}}
					className={classes.snackbar}
					open={open}
					autoHideDuration={2000}
					onClose={() => setOpen(false)}
					ContentProps={{
						'aria-describedby': 'Criado com sucesso!',
					}}
					message={message}
				/>
			</Container>

			<Fab color='primary' aria-label='salvar' className={classes.add} onClick={id ? updateClick : addClick}>
				<Done />
			</Fab>

		</MuiPickersUtilsProvider>
	);
}

const useStyles = makeStyles(theme => ({
	container: {
		paddingBottom: theme.spacing(10),
	},
	title: {
		flexGrow: 1,
	},
	add: {
		position: 'fixed',
		bottom: theme.spacing(8),
		right: theme.spacing(2),
	},
	snackbar: {
		bottom: theme.spacing(16),
	},
}));
