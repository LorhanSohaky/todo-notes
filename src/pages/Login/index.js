import React, { useState } from 'react';
import { Button, TextField, Box, Container, Grid, Link } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

import { login } from '../../providers/user';

export default function Login({ history }) {
	const classes = useStyles();

	const [email, setEmail] = useState(null);
	const [senha, setSenha] = useState(null);

	function handleSubmit(event) {
		event.preventDefault();
		login({ email, senha }).then(response => {
			if (response.logado) {
				history.push('/');
			}
		});

	}

	return (
		<Box height='100vh' display='flex' flexDirection='column' alignItems='center' justifyContent='center'>
			<Box display='flex' flexDirection='column' alignItems='center' justifyContent='space-between' >
				<img src='logo128.png' alt='logo' width='128' />
				<Container>
					<form onSubmit={handleSubmit}>
						<TextField margin='normal' required fullWidth type='email' label='E-mail' onChange={event => setEmail(event.target.value)} />
						<TextField margin='normal' required fullWidth label='Senha' type='password' onChange={event => setSenha(event.target.value)} />
						<Button type='submit' fullWidth variant='contained' className={classes.submit} color='primary'>Acessar</Button>
					</form>
					<Grid container>
						<Grid item>
							<Link href='/register' variant='body2'>
								{'Não tem uma conta? Crie uma agora'}
							</Link>
						</Grid>
					</Grid>
				</Container>
			</Box>
		</Box>
	);
}

const useStyles = makeStyles(theme => ({
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));