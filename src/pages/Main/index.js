import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';

import ListNotes from '../ListNotes/index.js';
import BottomMenu from '../../components/BottomMenu/index.js';

export default function Main({ history }) {
	const classes = useStyles();
	const [active, setActive] = useState('pendentes');

	function handleEvent(newValue) {
		setActive(newValue);
	}

	return (
		<div className={classes.app}>
			<div className={classes.content}>
				{(active === 'pendentes') && <ListNotes history={history} filtrar={false} />}
				{(active === 'concluidos') && <ListNotes history={history} filtrar={true} />}
			</div>
			<div className={classes.menu}>
				<BottomMenu default_state={active} onChange={handleEvent} />
			</div>
		</div>
	);
}


const useStyles = makeStyles(theme => ({
	app: {
		textAlign: 'center',
		height: '100vh',
	},
	content: {
		width: '100%',
		height: 'calc(100% - 56px)',
	},

	menu: {
		bottom: '0',
		height: '56',
		width: '100%',
	},
}));