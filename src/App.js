import React, { useState, useEffect } from 'react';
import './App.css';

import { getLogado } from './providers/user';

import Main from './pages/Main';
import Login from './pages/Login';

function App({ history }) {

	const [logado, setLogado] = useState(false);

	async function isLogado() {
		getLogado().then(response => {
			if (response.status === 200) {
				setLogado(JSON.parse(response.logado));
			}
		});
	}

	useEffect(() => {
		isLogado();
	});

	return (
		<div>
			{(logado === true) && <Main history={history} />}
			{(logado === false) && <Login history={history} />}
		</div>
	);
}

export default App;
