export async function newNote(note) {

	let notes = JSON.parse(await localStorage.getItem('notes')) || [];
	note = JSON.stringify({ 'id': notes.length + 1, ...note, 'concluido': false });
	notes.push(note);

	await localStorage.setItem('notes', JSON.stringify(notes));

	return { 'status': 201 };
}

export async function listNotes() {
	let notes = JSON.parse(await localStorage.getItem('notes')) || [];

	return { 'status': 200, notes };
}

export async function checkNote(id) {
	let notes = JSON.parse(await localStorage.getItem('notes')) || [];

	notes = notes.map((note) => {
		note = JSON.parse(note);

		if (note.id === id) {
			note.concluido = !note.concluido;
		}
		return JSON.stringify(note);
	});

	await localStorage.setItem('notes', JSON.stringify(notes));

	return { 'status': 200, notes };
}


export async function getNote(id) {
	id = Number(id);
	let notes = JSON.parse(await localStorage.getItem('notes')) || [];

	let note = notes.filter((item) => {
		item = JSON.parse(item);
		return item.id === id;
	});

	return { 'status': 200, note };
}

export async function updateNote(id, newNote) {
	id = Number(id);
	let notes = JSON.parse(await localStorage.getItem('notes')) || [];

	let note = notes.filter((item) => {
		item = JSON.parse(item);
		return item.id === id;
	});

	note = JSON.parse(note);

	notes = notes.filter((item) => {
		item = JSON.parse(item);
		return item.id !== id
	});


	notes.push(JSON.stringify({ 'id': id, ...newNote, 'concluido': note.concluido }));
	console.log(notes);
	await localStorage.setItem('notes', JSON.stringify(notes));

	return { 'status': 200, notes };
}
