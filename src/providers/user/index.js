export async function registerUser(user) {
	await localStorage.setItem('user', JSON.stringify(user));

	return { 'status': 201 };
}

export async function getLogado() {
	let logado = await localStorage.getItem('logado') || false;

	return { 'status': 200, logado };
}

export async function login(credentials) {
	const user = JSON.parse(await localStorage.getItem('user'));
	const logado = user.email === credentials.email && user.senha === credentials.senha;

	await localStorage.setItem('logado', logado);

	return { 'status': 200, logado };
}