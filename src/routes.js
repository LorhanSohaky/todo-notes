import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './App';
import NewNote from './pages/NewNote';
import Register from './pages/Register';


export default function Routes() {
	return (
		<BrowserRouter>
			<Route path="/" exact component={App} />
			<Route path="/register" component={Register} />
			<Route path="/new-note" component={NewNote} />
			<Route path="/edit-note/:id" component={NewNote} />
		</BrowserRouter>
	);
}