import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#FFAB00',
			light: '#ffdd4b',
			dark: '#c67c00',
		},
		secondary: {
			main: '#ff6f00',
			light: '#ffa040',
			dark: '#c43e00',
		},
		background: {
			default: '#FFF8E1'
		},
	},
});