import React from 'react';

import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';

function BottomMenu({ default_state, onChange }) {
	const [value, setValue] = React.useState(default_state);

	function handleChange(event, newValue) {
		setValue(newValue);
		onChange(newValue);
	}

	return (
		<BottomNavigation value={value} onChange={handleChange} showLabels>
			<BottomNavigationAction label='Pendentes' value='pendentes' />
			<BottomNavigationAction label='Concluídos' value='concluidos' />
		</BottomNavigation>
	);
}

export default BottomMenu;