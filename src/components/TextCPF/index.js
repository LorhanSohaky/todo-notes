import React from 'react';

import { FormControl, Input, InputLabel } from '@material-ui/core';

import MaskedInput from 'react-text-mask';
import PropTypes from 'prop-types';

export default function TextCPF(props, { onChange, value }) {
	return (
		<FormControl {...props}>
			<InputLabel >CPF</InputLabel>
			<Input
				value={value}
				onChange={onChange}
				inputComponent={MaskCPF}
			/>
		</FormControl>
	);
}

function MaskCPF(props) {
	const { inputRef, ...other } = props;

	return (
		<MaskedInput
			{...other}
			ref={ref => {
				inputRef(ref ? ref.inputElement : null);
			}}
			mask={[/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/]}
			placeholderChar={'\u2000'}
		/>
	);
}

MaskCPF.propTypes = {
	inputRef: PropTypes.func.isRequired,
};